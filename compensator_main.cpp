/*
 *  Copyright (c) 2020, The Linux Foundation. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are
 *  met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 *  ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 *  BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 *  BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 *  OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 *  IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <array>
#include <iostream>
#include <csignal>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <errno.h>
#include <fcntl.h>
#include <grp.h>
#include <pthread.h>
#include <unistd.h>
#include <pid_lock.h>
#include "compensator_log.h"
#include "compensator.hpp"

#define PID_FILE          ("/var/run/compensator.pid")

static CompensatorDaemon c_daemon;
static std::string config_path;
static bool foreground = false;
static bool quit = false;
static bool do_cableloss = false;
const char usageStr[] =
    "************************************************************************\n"
    "A compensator daemon.\n"
    "\n"
    "Usage:\n"
    "\n"
    "  ./compensator [options]\n"
    "\n"
    "  -f [config file path]    Specify config file path. If path is unspecified \n"
    "                    it defaults to /etc/compensator.conf\n"
    "  -h              Print this message\n"
    "  -p              Run as foreground program\n"
    "  -q              Kill currently running instance of daemon\n"
    "  -c              Start cable loss measurement mode\n"
    "\n"
    "************************************************************************\n"
    ;

static inline void printUsageExit()
{
    std::cout << usageStr << std::endl;
    exit(0);
}

static void parseCommandline(int argc, char *argv[])
{
    int c;

    while ((c = getopt(argc, argv, "hpqcf:")) != -1) {
        switch (c) {
        case 'f':
            if (optarg) {
                config_path = std::string(optarg);
            }
            break;
        case 'p':
            foreground = true;
            break;
        case 'q':
            quit = true;
            break;
        case 'c':
            do_cableloss = true;
            break;
        case 'h':
            printUsageExit();
        case '?':
            if (optopt == 'f') {
                std::cout << "option -f requires argument: config file path" << std::endl;
            } else if (optopt == 'c') {
                std::cout << "opton -c requires argument: xml config file path" << std::endl;
            } else if (isprint(optopt)) {
                std::cout << "unknown option '-'" << optopt << std::endl;
            }
            printUsageExit();
            break;
        default:
            abort();
        }
    }
}

/*******************************************************************************
 * capture the value of terminate signal, for a graceful termination.
 *
 * @param
 ******************************************************************************/
static void sigterm_handler(int sig)
{
    CP_INFO("Got signal\n");
    c_daemon.stop();
}

static void install_handler(sighandler_t signal_handler)
{
    std::array<int, 3> handled_sigs = {SIGHUP, SIGTERM, SIGQUIT};
    sigset_t blockset;

    sigemptyset(&blockset);

    for (auto i = handled_sigs.begin(); i != handled_sigs.end(); ++i) {
        sigaddset(&blockset, *i);
        signal(*i, signal_handler);
    }
}
/*******************************************************************************
 * create a true daemon with no controlling tty. set the working directory
 * and file-system mask.
 * Has similarity to bsd daemon(), but conforming to POSIX
 *
 * @param signal_handler : signal handler for graceful termination. This blocks
 *  the signals until the future call to pselect(). NULL if not interested in the
 *  support.
 *
 * @return int : On successful return from this subroutine is in the
 * daemon-ized process.
 ******************************************************************************/
int daemonize(sighandler_t signal_handler)
{
    pid_t daemon_pid;

    daemon_pid = fork();
    if (-1 == daemon_pid) {
        return errno;
    }
    if (0 < daemon_pid) {
        exit(EXIT_SUCCESS); /* orphan the daemon */
    }

    /* auto-reap zombies */
    signal(SIGCHLD, SIG_IGN);

    if (NULL != signal_handler) {
        install_handler(signal_handler);
    }

    return 0;
}

int main(int argc, char *argv[])
{
    int rc = 0;
    int pid_lock = -1;
    int pid;

    parseCommandline(argc, argv);

    /* Are we just stopping the daemon? */
    if (quit) {
        /* make sure the daemon exists */
        rc = pidfile_acquire(PID_FILE, &pid, &pid_lock);
        if (EEXIST != rc) {
            CP_ERR("daemon not found, probed %s\n", PID_FILE);
            rc = ESRCH;
        } else {
            rc = kill(pid, SIGTERM);
            if (-1 == rc) {
                rc = errno;
            }
        }
        goto bail;
    }

    /* Shall we transform in to daemon process? */
    if (foreground == false) {
        /* run as daemon. on a successful return the run-time will be in
           daemon process */
        rc = daemonize(sigterm_handler);
    } else {
        install_handler(sigterm_handler);
    }

    if (!rc) {
        /* make sure there is atmost one instance of qcamdaemon */
        rc = pidfile_acquire(PID_FILE, &pid, &pid_lock);
        if (EEXIST == rc) {
            CP_ERR("pid file %s lockd by %d\n", PID_FILE, pid);
            goto bail;
        }
    }
    /* now run the actual program. this must use pselect() or equivalent
       to turn on signals */
    if (config_path.empty() == false)
        rc = c_daemon.init(config_path);
    else
        rc = c_daemon.init();

    if (!rc) {
        c_daemon.run(do_cableloss);
    }

bail:
    pidfile_release(PID_FILE, &pid_lock);
    return rc;
}
