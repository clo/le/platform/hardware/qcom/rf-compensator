/*
 *  Copyright (c) 2020, The Linux Foundation. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are
 *  met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 *  ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 *  BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 *  BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 *  OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 *  IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <regex>
#include <thread>
#include <errno.h>
#include <fcntl.h>
#include <termios.h>
#include <unistd.h>
#include <stdlib.h>
#include <dirent.h>
#include <sys/select.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/time.h>
#include <signal.h>
#include "compensator_log.h"
#include "compensator.hpp"

using telux::common::ErrorCode;
using telux::common::Status;
using telux::cv2x::Cv2xFactory;
using telux::cv2x::Cv2xStatus;
using telux::cv2x::Cv2xStatusType;

std::promise<telux::common::ErrorCode> CompensatorDaemon::callbackPromise_ = std::promise<ErrorCode>();

CompensatorDaemon::CompensatorDaemon()
{
    baudMap_["9600"] = B9600;
    baudMap_["19200"] = B19200;
    baudMap_["38400"] = B38400;
    baudMap_["57600"] = B57600;
    baudMap_["115200"] = B115200;

    statusStringMap_[CompensatorStatus::STATUS_OK] = std::string("OK");
    statusStringMap_[CompensatorStatus::STATUS_COUNT] = std::string("Packet Count");
    statusStringMap_[CompensatorStatus::STATUS_TEMP] = std::string("Temperature");
    statusStringMap_[CompensatorStatus::STATUS_TEMP_OUT_OF_RANGE] =
        std::string("Temperature out of range");
    statusStringMap_[CompensatorStatus::STATUS_CABLELOSS_OUT_OF_RANGE] =
        std::string("Cable loss out of range");
    statusStringMap_[CompensatorStatus::STATUS_TX_POWER_LOW] = std::string("Tx power low");
    statusStringMap_[CompensatorStatus::STATUS_TX_POWER_HIGH] = std::string("Tx power high");
    statusStringMap_[CompensatorStatus::STATUS_MISSING_CAL] = std::string("Missing cal table");
    statusStringMap_[CompensatorStatus::STATUS_FW_FAILURE] = std::string("FW failure");
    statusStringMap_[CompensatorStatus::STATUS_CABLELOSS_IN_PROGRESS] =
        std::string("Cable loss in progress");
    statusStringMap_[CompensatorStatus::STATUS_CABLELOSS_DONE_OK] =
        std::string("Cable loss success");
    statusStringMap_[CompensatorStatus::STATUS_CABLELOSS_DONE_FAIL] =
        std::string("Cable loss failed");
    statusStringMap_[CompensatorStatus::STATUS_FREQ_REQUEST] = std::string("Freq ind request");
    statusStringMap_[CompensatorStatus::STATUS_UNKNOWN] = std::string("Unknown status code");

    Fd_[0] = Fd_[1] = -1;
    rxToCount_[0] = rxToCount_[1] = 0;
    clToCount_[0] = clToCount_[1] = 0;
    CauseCode_[0] = CauseCode_[1] = TerminateCause::DAEMON_STOP;
    State_[0] = State_[1] = CompensatorState::UNKNOWN;
    stop_ = false;
}

CompensatorDaemon::~CompensatorDaemon()
{
    deinit();
}
/**
 * Utility function to read config file with key value pairs
 * Prepares a map of key value pairs from Key=Value format
 * Discards leading spaces, blank lines and lines starting with #
 * Removes any leading or trailing spaces around Key and Value if any.
 */
void CompensatorDaemon::readConfigFile(std::string configFile) {

  // Create a file stream from the file name
  std::ifstream configFileStream(configFile);

  // Iterate through each parameter in the file and read the key value pairs
  std::string param;
  while (std::getline(configFileStream >> std::ws, param)) {
    std::string key;
    std::istringstream paramStream(param);
    if (std::getline(paramStream, key, '=')) {
      // Ignore lines starting with # character
      if (key[0] == '#') {
        continue;
      }
      key = std::regex_replace(key, std::regex(" +$"), "");
      if (key.length() > 0) {
        std::string value;
        if (std::getline(paramStream, value)) {
          value = std::regex_replace(value, std::regex("^ +| +$"), "");
          configMap_[key] = value;
        }
      }
    }
  }
}

std::string CompensatorDaemon::getConfigValue(std::string key) {
    auto settingsIterator = configMap_.find(key);
    if (settingsIterator != configMap_.end()) {
        return settingsIterator->second;
    } else {
        return std::string(""); // return an empty string when the setting is not configured.
    }
}

int CompensatorDaemon::openDevice(int id)
{
    int fd, ret;
	struct termios tty;
    const char *devName;

    if (id == PRX_ID) {
        devName = Config_.PrxUartName.c_str();
    } else {
        devName = Config_.DrxUartName.c_str();
    }
    fd = open(devName, O_RDWR | O_NOCTTY | O_SYNC);
    if (fd < 0) {
        CP_ERR("Failed to open device %s\n", devName);
        return -1;
    }
    /*disable the clock then enable it, to make sure the clock count is 1*/
    ret = ioctl(fd, MSM_DISABLE_UART_CLOCK, 0);
    ret = ioctl(fd, MSM_ENABLE_UART_CLOCK, 0);
    if (ret < 0){
        CP_ERR("Failed to enable MSM clock\n");
        return -1;
    }

    /* Set interface attributes */
    if (tcgetattr(fd, &tty) < 0) {
        CP_ERR("Failed to get interface attribute\n");
        return -1;
    }
    auto bauditer = baudMap_.find(Config_.BaudRate);
    if (bauditer == baudMap_.end()) {
        CP_ERR("Invalid baudrate: %s\n", Config_.BaudRate.c_str());
        return -1;
    }
    CP_INFO("Opening %s at %s\n", devName, Config_.BaudRate.c_str());
    auto speed = bauditer->second;
    cfsetospeed(&tty, (speed_t)speed);
    cfsetispeed(&tty, (speed_t)speed);

    /* Ignore modem control, enable receiver */
    tty.c_cflag |= (CLOCAL | CREAD);
    tty.c_cflag &= ~(CSIZE | PARENB);
    /* Set to 8N1 and no hardware flowcontrol */
    tty.c_cflag |= CS8;
    tty.c_cflag &= ~CSTOPB;
    tty.c_cflag &= ~CRTSCTS;

    /* setup for raw mode */
    tty.c_iflag &= ~(IGNBRK | BRKINT | PARMRK | ISTRIP | INLCR | IGNCR | ICRNL | IXON);
    tty.c_oflag &= ~OPOST;
    tty.c_lflag &= ~(ECHO | ECHONL | ICANON | ISIG | IEXTEN);

    /* Timed read, note the VTIME is in 1/10 second unit */
    tty.c_cc[VMIN] = 0;
    tty.c_cc[VTIME] = 10;

    if (tcsetattr(fd, TCSANOW, &tty) != 0) {
        CP_ERR("Failed to set attribute\n");
        return -1;
    }

    tcflush(fd, TCIOFLUSH);
    return fd;
}
int CompensatorDaemon::setupGPIO(int gpio)
{
    std::string cmd("/sys/class/gpio/gpio" + std::to_string(gpio));
    DIR* dir = opendir(cmd.c_str());
    if (!dir) {
        if (ENOENT == errno) {
            cmd.clear();
            cmd.assign("echo " + std::to_string(gpio) +  " > /sys/class/gpio/export");
            system(cmd.c_str());
        } else {
            CP_ERR("Error when setting up GPIO\n");
            return -EIO;
        }
    } else {
        closedir(dir);
    }
    cmd.assign("echo out > /sys/class/gpio/gpio" + std::to_string(gpio) + "/direction");
    system(cmd.c_str());
    return 0;
}
int CompensatorDaemon::writeGPIO(int gpio, int value)
{
    std::string cmd("echo " + std::to_string(value) +
            " > /sys/class/gpio/gpio" + std::to_string(gpio) + "/value");
    system(cmd.c_str());
    return 0;
}
void CompensatorDaemon::powerOnOff(int id, bool on)
{
    if (id == PRX_ID) {
        writeGPIO(Config_.PrxPonGPIO, (on == true? 1: 0));
    } else {
        writeGPIO(Config_.DrxPonGPIO, (on == true? 1: 0));
    }
    State_[id] = (on == true) ? CompensatorState::ON : CompensatorState::OFF;
}

int CompensatorDaemon::init(std::string configFile)
{
    std::ifstream f(configFile.c_str());
    if (f.good()) {
        readConfigFile(configFile);
    } else {
        CP_ERR("Can't find config file: %s\n", configFile.c_str());
        return -1;
    }
    try {
        Config_.PrxPonGPIO = std::stoi(getConfigValue("PrxPonGPIO"));
    } catch (std::invalid_argument& e) {
        CP_ERR("Invalid or missing config item PrxPonGPIO\n");
        return -1;
    }
    try {
        Config_.DrxPonGPIO = std::stoi(getConfigValue("DrxPonGPIO"));
    } catch (std::invalid_argument& e) {
        CP_ERR("Invalid or missing config item DrxPonGPIO\n");
        return -1;
    }
    try {
        Config_.AntennaBreakPwrGPIO = std::stoi(getConfigValue("AntennaBreakPwrGPIO"));
    } catch (std::invalid_argument& e) {
        CP_ERR("Invalid or missing config item AntennaBreakPwrGPIO\n");
        return -1;
    }
    Config_.PrxUartName = getConfigValue("PrxUartName");
    Config_.DrxUartName = getConfigValue("DrxUartName");
    Config_.BaudRate = getConfigValue("BaudRate");

    auto baudIter = baudMap_.find(Config_.BaudRate);
    if (baudIter == baudMap_.end()) {
        CP_ERR("Invalid baudrate in config file\n");
        return -1;
    }

    if (!getConfigValue("StopOnTemperatureLimit").compare("true")) {
        Config_.StopOnTemperatureLimit = true;
    } else {
        Config_.StopOnTemperatureLimit = false;
    }
    if (!getConfigValue("StopOnCableLossLimit").compare("true")) {
        Config_.StopOnCableLossLimit = true;
    } else {
        Config_.StopOnCableLossLimit = false;
    }
    if (!getConfigValue("StopOnPowerLowLimit").compare("true")) {
        Config_.StopOnPowerLowLimit = true;
    } else {
        Config_.StopOnPowerLowLimit = false;
    }
    if (!getConfigValue("StopOnPowerHighLimit").compare("true")) {
        Config_.StopOnPowerHighLimit = true;
    } else {
        Config_.StopOnPowerHighLimit = false;
    }
    if (!getConfigValue("PrxActive").compare("true")) {
        Config_.PrxActive = true;
    }
    if (!getConfigValue("DrxActive").compare("true")) {
        Config_.DrxActive = true;
    }
    std::string band = getConfigValue("Band");
    if (!band.compare("HB")) {
        Config_.Band = CompensatorFreq::HB_FREQ;
    } else if (!band.compare("MB")) {
        Config_.Band = CompensatorFreq::MB_FREQ;
    } else if (!band.compare("LB")) {
        Config_.Band = CompensatorFreq::LB_FREQ;
    } else {
        CP_ERR("Invalid Band in config file\n");
        return -1;
    }

    Config_.MissionModeConfigFilePrx = getConfigValue("MissionModeConfigFilePrx");
    Config_.MissionModeConfigFileDrx = getConfigValue("MissionModeConfigFileDrx");
    Config_.MissionModeConfigFile = getConfigValue("MissionModeConfigFile");
    Config_.TrainingModeConfigFile = getConfigValue("TrainingModeConfigFile");
    if (Config_.MissionModeConfigFile.empty()) {
        CP_INFO("WARNING: missing MissionModeConfigFile name in config file\n");
    }
    if (Config_.TrainingModeConfigFile.empty()) {
        CP_INFO("WARNING: missing TrainingModeConfigFile name in config file\n");
    }
    Config_.TrainingCommand = getConfigValue("TrainingCommand");

    /* export gpios and power on compensator */
#if 0
    setupGPIO(ANT_BREAK_PWR_EN);
#endif
    if (Config_.PrxActive == true) {
        setupGPIO(Config_.PrxPonGPIO);
        powerOnOff(PRX_ID, true);
    }
    if (Config_.DrxActive == true) {
        setupGPIO(Config_.DrxPonGPIO);
        powerOnOff(DRX_ID, true);
    }

    return 0;
}
void CompensatorDaemon::deinit(void)
{
    if (Config_.PrxActive == true) {
        powerOnOff(PRX_ID, false);
    }
    if (Config_.DrxActive == true) {
        powerOnOff(DRX_ID, false);
    }
}
CompensatorStatus CompensatorDaemon::decodeStatus(int id, char *buf, bool &condition)
{
    CompensatorStatus status;
    std::string sstring;
    sstring += "Compensator " + std::to_string(id) + ":";

    if (IS_VALID(buf) == false) {
        sstring += ": Invalid status frame";
        status = CompensatorStatus::STATUS_UNKNOWN;
    } else {
        status = static_cast<CompensatorStatus>(buf[0]);
        auto  statusStringIter = statusStringMap_.find(status);
        if (statusStringIter == statusStringMap_.end()) {
            sstring += ": invalid status code" + std::to_string(buf[0]);
            status = CompensatorStatus::STATUS_UNKNOWN;
        } else {
            sstring += statusStringIter->second;
            switch(status) {
                case CompensatorStatus::STATUS_OK:
                case CompensatorStatus::STATUS_COUNT:
                case CompensatorStatus::STATUS_TEMP:
                    sstring += ": " + std::to_string(buf[1]);
                    break;
                case CompensatorStatus::STATUS_TEMP_OUT_OF_RANGE:
                    sstring += ": " + std::to_string(buf[1]);
                    if (Config_.StopOnTemperatureLimit == true) {
                        condition = true;
                    }
                    break;
                case CompensatorStatus::STATUS_CABLELOSS_OUT_OF_RANGE:
                    sstring += ": " + std::to_string(buf[1]);
                    if (Config_.StopOnCableLossLimit == true) {
                        condition = true;
                    }
                    break;
                case CompensatorStatus::STATUS_TX_POWER_LOW:
                    sstring += ": " + std::to_string(buf[1]);
                    if (Config_.StopOnPowerLowLimit == true) {
                        condition = true;
                    }
                    break;
                case CompensatorStatus::STATUS_TX_POWER_HIGH:
                    sstring += ": " + std::to_string(buf[1]);
                    if (Config_.StopOnPowerHighLimit == true) {
                        condition = true;
                    }
                    break;
                case CompensatorStatus::STATUS_CABLELOSS_DONE_OK:
                    std::cout << std::to_string((int8_t)buf[1]);
                    break;
                default:
                    break;
            }
            std::cout << std::endl;
        }
    }
    CP_INFO("%s\n", sstring.c_str());
    return status;
}
bool CompensatorDaemon::isCablelossReady(CompensatorStatus status)
{
    return (status != CompensatorStatus::STATUS_FW_FAILURE &&
        status != CompensatorStatus::STATUS_MISSING_CAL &&
        status != CompensatorStatus::STATUS_TEMP_OUT_OF_RANGE &&
        status != CompensatorStatus::STATUS_UNKNOWN);
}
void CompensatorDaemon::terminateCableloss(bool stopDaemon)
{
    callbackPromise_ = std::promise<ErrorCode>();
    if (updateConfiguration(Config_.MissionModeConfigFile)) {
        CP_ERR("Failed to restore missiong mode config file\n");
    }
    if (stopDaemon == true) {
        CP_INFO("Stopping daemon\n");
        stop();
    }
}
void CompensatorDaemon::threadMain(int id, bool cableLoss)
{
    int rdlen;
    char buf[STATUS_FRAME_LEN];
    bool condition;
    int fd = openDevice(id);

    if (fd < 0) {
        CP_ERR("Compensator %d device open error\n", id);
        CauseCode_[id] = TerminateCause::DEVICE_ERROR;
        return;
    } else {
        Fd_[id] = fd;
    }

    /* Send command to compensator to indicate the frequency band we are operating at */
    if (sendCommand(id, CompensatorCommand::COMMAND_FREQ)) {
        CP_ERR("Compensator %d failed to send command to device\n", id);
        CauseCode_[id] = TerminateCause::DEVICE_ERROR;
        return;
    }
    if (cableLoss == true) {
        cablelossState_ = CablelossState::COMMAND_START;
    }

    while(stop_ == false) {
        rdlen = read(fd, buf, STATUS_FRAME_LEN);
        if (rdlen < 0) {
            if (EINTR == errno) {
                CP_ERR("Compensator %d read interrupted\n", id);
            } else {
                CP_ERR("Compensator %d read error\n", id);
            }
        } else if (rdlen == 0) {
            rxToCount_[id]++;
            /*
             * If compensator didn't send status report in 2 consecutive sequence, declare
             * connection loss.
             */
            if (rxToCount_[id] >= 2*STATUS_REPORT_PERIOD) {
                if (cableLoss == true && cablelossState_ != CablelossState::COMMAND_COMPLETED) {
                    CP_ERR("Compensator %d timedout waiting for CLM message\n", id);
                    terminateCableloss(true);
                    continue;
                } else {
                    CP_ERR("Compensator %d lost connection with device\n", id);
                    CauseCode_[id] = TerminateCause::COMM_LOST;
                    return;
                }
                rxToCount_[id] = 0; //reset timeout count
            }
        } else {
            if (rdlen != STATUS_FRAME_LEN) {
                continue;
            }
            rxToCount_[id] = 0; //reset timeout count
            condition = false;
            Status_[id] = decodeStatus(id, buf, condition);
            // If stop condition is met, return immediately
            if (condition == true) {
                CauseCode_[id] = TerminateCause::CONDITION_MET;
                return;
            }
            if (Status_[id] == CompensatorStatus::STATUS_FREQ_REQUEST) {
                /* Send HB/LB indication upon request */
                if (sendCommand(id, CompensatorCommand::COMMAND_FREQ)) {
                    CauseCode_[id] = TerminateCause::DEVICE_ERROR;
                    return;
                }
                continue;
            }
            if (cableLoss == true) {
                if (isCablelossReady(Status_[id]) == false) {
                    CP_INFO("Compensator %d is not ready for CLM\n", id);
                    terminateCableloss(true);
                    continue;
                }
                if (clToCount_[id] >= 5) {
                    CP_INFO("Compensator %d too many unexpected status received for CLM\n", id);
                    terminateCableloss(true);
                    continue;
                }
                switch(cablelossState_) {
                    case CablelossState::COMMAND_START:
                        if (sendCommand(id, CompensatorCommand::COMMAND_CABLE_LOSS)) {
                            CP_ERR("Compensator %d Failed to send command to device\n", id);
                            terminateCableloss(true);
                        } else {
                            CP_INFO("Compensator %d sent command to start CLM\n", id);
                            cablelossState_ = CablelossState::COMMAND_SENT;
                            clToCount_[id] = 0; /*start counting the timeout */
                        }
                        break;
                    case CablelossState::COMMAND_SENT:
                        if (Status_[id] == CompensatorStatus::STATUS_CABLELOSS_IN_PROGRESS) {
                            /* Send training packets */
                            system(Config_.TrainingCommand.c_str());
                            cablelossState_ = CablelossState::RESPONSE_RECEIVED;
                        } else if (Status_[id] == CompensatorStatus::STATUS_CABLELOSS_PREPARE_FAIL){
                            CP_ERR("Compensator %d CLM prepare failed\n", id);
                            terminateCableloss(true);
                        } else {
                            /* Received other status code then expected */
                            clToCount_[id]++;
                        }
                        break;
                    case CablelossState::RESPONSE_RECEIVED:
                        if (Status_[id] == CompensatorStatus::STATUS_CABLELOSS_DONE_OK) {
                            CP_INFO("Compensator %d CLM success!\n", id);
                            terminateCableloss(false);
                            cablelossState_ = CablelossState::COMMAND_COMPLETED;
                        } else if (Status_[id] == CompensatorStatus::STATUS_CABLELOSS_DONE_FAIL) {
                            CP_ERR("Compensator %d CLM failed!\n", id);
                            std::cout << "Compensator " << id << " Cableloss failed!" << std::endl;
                            terminateCableloss(true);
                        } else {
                            /* Received other status code then expected */
                            clToCount_[id]++;
                        }
                        break;
                    default:
                        break;
                }
            }
        }
    }
}
int CompensatorDaemon::sendCommand(int id, CompensatorCommand cmd)
{
    char frame[STATUS_FRAME_LEN];
    frame[0] = static_cast<char>(cmd);
    if (cmd ==  CompensatorCommand::COMMAND_FREQ) {
        frame[1] = static_cast<char>(Config_.Band);
    } else if (cmd == CompensatorCommand::COMMAND_CABLE_LOSS) {
        frame[1] = frame[0];
    } else {
        CP_ERR("Command is invalid\n");
        return -1;
    }
    frame[2] = CSUM(frame);

    if (Fd_[id] > 0) {
        if (write(Fd_[id], frame, STATUS_FRAME_LEN) != STATUS_FRAME_LEN) {
            CP_ERR("Write failed for compensator %d\n", id);
            return -1;
        }
    } else {
        CP_ERR("Invalid Fd for compensator %d\n", id);
        return -1;
    }
    return 0;
}
void CompensatorDaemon::updateConfigurationCallback(ErrorCode error)
{
    callbackPromise_.set_value(error);
}
int CompensatorDaemon::updateConfiguration(std::string configFile)
{
    auto & cv2xFactory = Cv2xFactory::getInstance();
    auto cv2xRadioManager = cv2xFactory.getCv2xRadioManager();

    // Wait for radio manager to complete initialization
    if (not cv2xRadioManager->isReady()) {
        if (!cv2xRadioManager->onReady().get()) {
            CP_ERR("C-V2X radio manager init failed\n");
            return EXIT_FAILURE;
        }
    }

    if (Status::SUCCESS != cv2xRadioManager->updateConfiguration(configFile,
                 CompensatorDaemon::updateConfigurationCallback)) {
        CP_ERR("Request for config file update failed\n");
        return EXIT_FAILURE;
    }
    if (ErrorCode::SUCCESS != callbackPromise_.get_future().get()) {
        CP_ERR("config file update failed\n");
        return EXIT_FAILURE;
    } else {
        CP_INFO("Configuration udpated\n");
    }
    return 0;
}
void CompensatorDaemon::run(bool cableLoss)
{
    if (cableLoss == true) {
        if (updateConfiguration(Config_.TrainingModeConfigFile)) {
            return;
        }
    }
    if (Config_.PrxActive == true) {
        Thread_[PRX_ID] = std::thread([this, cableLoss]() { threadMain(PRX_ID, cableLoss);});
    }
    if (Config_.DrxActive == true) {
        Thread_[DRX_ID] = std::thread([this, cableLoss]() { threadMain(DRX_ID, cableLoss);});
    }
    do {
        if (State_[PRX_ID] == CompensatorState::ON) {
            if (stop_ == true) {
                Thread_[PRX_ID].join();
                powerOnOff(PRX_ID, false);
            } else if (CauseCode_[PRX_ID] != TerminateCause::DAEMON_STOP) {
                //PRX thread terminated for some reason, power it off, and set Tx Diversity to Drx
                Thread_[PRX_ID].join();
                powerOnOff(PRX_ID, false);
                CP_INFO("Prx Compensator Powered OFF\n");
                if (!Config_.MissionModeConfigFileDrx.empty() && Config_.DrxActive 
                        && (State_[DRX_ID] == CompensatorState::ON)) {
                    if (updateConfiguration(Config_.MissionModeConfigFileDrx)) {
                        CP_ERR("Failed to load config file\n");
                    } else {
                        CP_INFO("Tx is on DRX ONLY\n");
                    }
                }
            }
        }
        if (State_[DRX_ID] == CompensatorState::ON) {
            if (stop_ == true) {
                Thread_[DRX_ID].join();
                powerOnOff(DRX_ID, false);
            } else if (CauseCode_[DRX_ID] != TerminateCause::DAEMON_STOP) {
                //PRX thread terminated for some reason, power it off, and set Tx Diversity to Drx
                Thread_[DRX_ID].join();
                powerOnOff(DRX_ID, false);
                CP_INFO("Drx compensator powered OFF\n");
                if (!Config_.MissionModeConfigFilePrx.empty() && Config_.PrxActive
                        && (State_[PRX_ID] == CompensatorState::ON)) {
                    if (updateConfiguration(Config_.MissionModeConfigFilePrx)) {
                        CP_ERR("Failed to load config file\n");
                    } else {
                        CP_INFO("Tx is on PRX ONLY\n");
                    }
                }
            }
        }
        sleep(1);
    } while(State_[PRX_ID] == CompensatorState::ON || State_[DRX_ID] == CompensatorState::ON);
}

void CompensatorDaemon::stop(void)
{
    stop_ = true;
    if (Config_.PrxActive == true) {
        if (Fd_[PRX_ID] > 0) {
            close(Fd_[PRX_ID]);
            Fd_[PRX_ID] = -1;
        }
    }
    if (Config_.DrxActive == true) {
        if (Fd_[DRX_ID] > 0) {
            close(Fd_[DRX_ID]);
            Fd_[DRX_ID] = -1;
        }
    }
}
