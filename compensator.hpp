/*
 *  Copyright (c) 2020, The Linux Foundation. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are
 *  met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 *  ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 *  BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 *  BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 *  OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 *  IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef _COMPENSATOR_HPP__
#define _COMPENSATOR_HPP__
#include <map>
#include <string>
#include <thread>
#include <telux/cv2x/Cv2xRadio.hpp>
#include <linux/version.h>

#define DEFAULT_CONFIG_FILE_NAME "/etc/compensator.conf"

#if (LINUX_VERSION_CODE >= KERNEL_VERSION(4,9,218))
	#define MSM_ENABLE_UART_CLOCK  	0x544D
	#define MSM_DISABLE_UART_CLOCK  0x544E
#else
	#define MSM_ENABLE_UART_CLOCK  	0x5441
	#define MSM_DISABLE_UART_CLOCK  0x5442
#endif

#define PRxCompensatorPon_GPIO  39
#define DRxCompensatorPon_GPIO  40
#define HB_LB_GPIO              89
#define ANT_BREAK_PWR_EN        47


#define STATUS_NO_OP		    0x20
#define STATUS_FRAME_LEN	    3
#define STATUS_REPORT_PERIOD    5
#define PRX_ID  0
#define DRX_ID  1

/* Status report: from compensator to TCU */
enum class CompensatorStatus {
	STATUS_OK = 0x40,
	STATUS_COUNT = 0x41,
	STATUS_TEMP = 0x54,
	STATUS_TEMP_OUT_OF_RANGE = 0x42,
	STATUS_CABLELOSS_OUT_OF_RANGE = 0x43,
	STATUS_TX_POWER_LOW = 0x44,
	STATUS_TX_POWER_HIGH = 0x45,
	STATUS_MISSING_CAL = 0x66,
	STATUS_FW_FAILURE = 0x68,
    STATUS_CABLELOSS_IN_PROGRESS = 0x69,
    STATUS_CABLELOSS_DONE_OK = 0x70,
    STATUS_CABLELOSS_DONE_FAIL = 0x71,
    STATUS_FREQ_REQUEST = 0x72,
    STATUS_CABLELOSS_PREPARE_FAIL = 0x73,
    STATUS_UNKNOWN = 0xFF,
};
 /* Command, from TCU to compensator */
enum class CompensatorCommand {
	COMMAND_CABLE_LOSS = 0x80,
    COMMAND_FREQ = 0x81,
    COMMAND_STOP = 0x82,
};

enum class CablelossState {
    COMMAND_START = 0,
    COMMAND_SENT = 1,
    RESPONSE_RECEIVED = 2,
    COMMAND_COMPLETED = 3,
};

enum class CompensatorState {
    UNKNOWN = 0,
    ON = 1,
    OFF = 2,
};

enum class TerminateCause {
    DAEMON_STOP = 0,
    DEVICE_ERROR = 1,
    COMM_LOST = 2,
    CONDITION_MET = 3,
};

#define SET_NO_OP(status) (status | STATUS_NO_OP)
#define IS_NO_OP(status) (status & STATUS_NO_OP)
#define IS_VALID(frame) ((0xFF - (0xFF & (frame[0] + frame[1]))) == frame[2])
#define CSUM(frame) (0xFF - (0xFF & (frame[0] + frame[1])))

enum class CompensatorFreq {
	HB_FREQ = 0,
	LB_FREQ = 1,
    MB_FREQ = 2,
	UNKNOWN_FREQ
};

struct CompensatorConfig {
    int PrxPonGPIO;
    int DrxPonGPIO;
    int AntennaBreakPwrGPIO;
    std::string PrxUartName;
    std::string DrxUartName;
    std::string BaudRate;
    std::string MissionModeConfigFile;
    std::string MissionModeConfigFilePrx;
    std::string MissionModeConfigFileDrx;
    std::string TrainingModeConfigFile;
    std::string TrainingCommand;
    bool StopOnTemperatureLimit;
    bool StopOnCableLossLimit;
    bool StopOnPowerLowLimit;
    bool StopOnPowerHighLimit;
    bool PrxActive;
    bool DrxActive;
    CompensatorFreq Band;
};

class CompensatorDaemon
{
public:
    CompensatorDaemon();
    ~CompensatorDaemon();
    int init(std::string configFile = DEFAULT_CONFIG_FILE_NAME);
    void run(bool cableLoss = false);
    void stop(void);
    void powerOnOff(int id, bool on);
    static std::promise<telux::common::ErrorCode> callbackPromise_;

private:
    void deinit(void);
    void readConfigFile(std::string configFile);
    std::string getConfigValue(std::string key);
    int openDevice(int id);
    int setupGPIO(int gpio);
    int writeGPIO(int gpio, int value);
    void threadMain(int id, bool cableLoss);
    int sendCommand(int id, CompensatorCommand cmd);
    CompensatorStatus decodeStatus(int id, char *buf, bool &condition);
    int updateConfiguration(std::string configFile);
    bool isCablelossReady(CompensatorStatus status);
    void terminateCableloss(bool stopDaemon);

    struct CompensatorConfig Config_;
    int Fd_[2];
    std::thread Thread_[2];
    int rxToCount_[2];
    int clToCount_[2];
    CompensatorStatus Status_[2];
    CompensatorState State_[2];
    TerminateCause CauseCode_[2];
    CablelossState cablelossState_;
    std::atomic<bool> stop_;
    std::map<std::string, int>baudMap_;
    std::map<std::string, std::string> configMap_;
    std::map<CompensatorStatus, std::string> statusStringMap_;
    static void updateConfigurationCallback(telux::common::ErrorCode error);

};
#endif
