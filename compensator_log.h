/*
 *  Copyright (c) 2020, The Linux Foundation. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are
 *  met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 *  ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 *  BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 *  BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 *  OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 *  IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef __COMPENSATOR_LOG_H__
#define __COMPENSATOR_LOG_H__

#include <stdio.h>
#include <syslog.h>

#ifdef __cplusplus
extern "C" {
#endif

#define CP_ERR(fmt, args...)  do {\
    syslog(LOG_ERR, "%s:%d ERROR: " fmt "\n", __func__, __LINE__, ##args);\
    fprintf(stderr, "%s:%d ERROR: " fmt "\n", __func__, __LINE__, ##args);\
} while (0)

#define CP_DBG(fmt, args...)  do {\
    syslog(LOG_DEBUG, "%s:%d DEBUG: " fmt "\n", __func__, __LINE__, ##args);\
    fprintf(stdout, "%s:%d DEBUG: " fmt "\n", __func__, __LINE__, ##args);\
} while (0)

#define CP_INFO(fmt, args...) do {\
    syslog(LOG_INFO, fmt "\n", ##args);\
    fprintf(stdout,  fmt "\n", ##args);\
} while (0)

#ifdef __cplusplus
}
#endif

#endif //__COMPENSATOR_LOG_H__
